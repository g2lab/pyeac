Overview
========

.. Give information about the motiviation and/or scientific backgroud.
.. Provide links to further information like scientific publications, presentation,
.. videos etc.

Choropleth maps are often used to display e.g. the results of an election. In such maps
it can happen that larger areas *seem* to have higher values because they are more
easily perceived by the viewer as a result of their size. Thus the main motivation for
generating **Equal Area Cartograms** is to produce a
representation of a spatial dataset that is not biased by the individual size of its
spatial entities.

The two images below are an example of what such a Equal Area Cartogram can look like:

.. image:: _static/nds_reg.png
    :width: 50%
    :alt: Regular grid representation

.. image:: _static/nds_hex.png
    :width: 48%
    :alt: Hexagonal grid representation

