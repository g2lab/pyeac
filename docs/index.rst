Equal Area Cartograms with python
=================================

Generate regular or hexagonal Equal Area Cartograms from polygon geometries of known geo formats
(GeoJSON, ESRI Shapefile, Geo-Package, tba).

.. Give some introductory words about the package/program and point
.. directly to the main modules/classes, i.e functionality that the package
.. provides.

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: Introduction

    overview.rst

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: User Guide

    install.rst
    usage.rst

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: Reference documentation

    api/index.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
