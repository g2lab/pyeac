{{ fullname | escape | underline }}

.. automodule:: {{ fullname }}
    :show-inheritance:
    :members:
