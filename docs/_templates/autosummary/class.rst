{{ fullname | escape | underline }}

.. currentmodule:: {{ module }}

.. autoclass:: {{ objname }}
    :show-inheritance:
    :members:


{% if methods|select("!=", "__init__")|reject("in", inherited_members)|list|length != 0 %}

.. rubric:: Methods Summary

.. autosummary::
    :toctree: ../../api/generated
    {% for item in methods %}
    {% if item != '__init__' and item not in inherited_members %}
    {{ objname }}.{{ item }}
    {% endif %}
    {% endfor %}

{% endif %}
