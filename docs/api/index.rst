.. _api:

API Reference
=============

.. In the API Reference write about the single modules that the package contains.
.. As autodoc already imports relevant parts be only specific when e.g. the instatiation
.. of a class is somewhat complicated and not self-explanatory.

.. The most basic outline just displays the current modules without any text:

.. currentmodule:: pyeac

.. autosummary::
    :toctree: generated

    core
    grid
    util

.. For a more in depth description use separate headings that describe single modules or
.. specific topics with more detail. The heading will show up in the sidebar and
.. provide a bit more structure, although further indenting the toc depth.

.. The core module
.. ---------------

.. .. autosummary::
..     :toctree: generated

..     core



