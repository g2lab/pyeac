""" Unit tests for the pyeac.core module """
import pytest
import shapely.geometry as sg

import pyeac.core

# Create a set of five geometries that are our geometric entities in a dataset
A = sg.Polygon([(1, 1), (1, 3.5), (3, 2)])
B = sg.Polygon([(1, 1), (3, 2), (3, 1)])
C = sg.Polygon([(3, 1), (3, 2), (4, 2.5), (4, 3.5), (6, 2.5)])
D = sg.Polygon([(1, 3.5), (1, 4.5), (4, 5), (4, 2.5), (3, 2)])
E = sg.Polygon([(1, 4.5), (1, 6), (4, 6), (4, 5)])
bounds = (1, 1, 6, 6)
geoms = [A, B, C, D, E]

# The parametrize decorator injects the above list of geoms to the fake_geoms argument
# of the fake_features fixture. This is a short and implicit version of the indirect
# parameter of pytest.mark.parametrize. It does not require to rework existing fixtures
# with the request fixture. See: https://stackoverflow.com/a/28570677


@pytest.mark.parametrize("fake_geoms", [geoms], ids=["fake_features"])
@pytest.mark.parametrize("gridtype", ["hex", "reg"])
class Test_geometries_to_grid_cells:
    def test_grid_cell_count_equals_feature_count(self, gridtype, fake_features):
        """
        geometries_to_grid_cells creates the same amount of grid cell geometries as
        there are original feature geometries
        """
        cells = pyeac.core.geometries_to_grid_cells(
            fake_features, bounds, gridtype=gridtype
        )
        assert len(cells) == len(geoms)

    def test_grid_cell_contained_in_original_features(self, gridtype, fake_features):
        """The new grid cell centroids are contained in the the original geometries"""
        cells = pyeac.core.geometries_to_grid_cells(
            fake_features, bounds, gridtype=gridtype
        )
        assert all(sg.MultiPolygon(geoms).contains(cell.centroid) for cell in cells)


@pytest.mark.parametrize("fake_geoms", [geoms], ids=["fake_features"])
@pytest.mark.parametrize("gridtype", ["hex", "reg"])
class Test_properties_to_grid_cells:
    def test_properties_count_equals_input_feature_count(self, gridtype, fake_features):
        """
        properties_to_grid_cells returns the same amount of features as there are
        original features.
        """
        cells = pyeac.core.geometries_to_grid_cells(
            fake_features, bounds, gridtype=gridtype
        )
        new_features = pyeac.core.properties_to_grid_cells(fake_features, cells)
        assert len(new_features) == len(fake_features)

    def test_properties_equal_input_features(self, gridtype, fake_features):
        """ The properties of the new features equal those of the original features."""
        cells = pyeac.core.geometries_to_grid_cells(
            fake_features, bounds, gridtype=gridtype
        )
        new_features = pyeac.core.properties_to_grid_cells(fake_features, cells)
        assert {str(nf["properties"]) for nf in new_features} == {
            str(ff["properties"]) for ff in fake_features
        }

    def test_raises_exception_when_feature_count_not_equal_grid_cell_count(
        self, gridtype, fake_features
    ):
        """
        The features parameter must have the same length as the grid parameter otherwise
        the assignment will not work.
        """
        cells = pyeac.core.geometries_to_grid_cells(
            fake_features, bounds, gridtype=gridtype
        )
        with pytest.raises(ValueError) as err:
            pyeac.core.properties_to_grid_cells(fake_features, cells[:-1])

        assert "'features' and 'grid' must have the same length: " in str(err.value)

    def test_raises_exception_when_cells_are_not_instances_of_GridCell(
        self, gridtype, fake_features
    ):
        """
        The grid parameter must contain only instances of GridCell otherwise the
        assignment based on the distance calculation cannot work.
        """
        cells = pyeac.core.geometries_to_grid_cells(
            fake_features, bounds, gridtype=gridtype
        )
        with pytest.raises(TypeError) as err:
            pyeac.core.properties_to_grid_cells(fake_features, cells[:-1] + [1])

        assert "Only instances of 'GridCell' can be used for the assignment!" in str(
            err.value
        )
