import pytest
import shapely.geometry as sg

import pyeac


@pytest.fixture
def centroids():
    """Provides a list of centroid coordinates arranged in a square"""
    return [(1, 1), (1, 5), (5, 5), (5, 1), (3, 3)]


@pytest.fixture
def fake_geoms(centroids):
    """
    Provides a list of square Polygons based on the coords of the centroids fixture
    """
    return [
        sg.Polygon(sg.Point(x, y).buffer(1).envelope.exterior.coords)
        for x, y in centroids
    ]


@pytest.fixture
def fake_features(fake_geoms):
    """
    Provides a list of features which are represented as a dictionary with the
    geometries (as mapping) of the fake_geoms fixture.
    """
    return [
        {
            "type": "Feature",
            "id": i,
            "properties": {"population": i * 10},
            "geometry": sg.mapping(geom),
        }
        for i, geom in enumerate(fake_geoms, start=1)
    ]


@pytest.fixture
def fake_geoms_w_multipolygons(fake_geoms):
    """
    Provides a list of square Polygon geometries of which the first two are a
    MultiPolygon based on the fake_geoms fixture.
    """
    multi = sg.MultiPolygon(fake_geoms[:2])
    return [multi] + fake_geoms[2:]


@pytest.fixture
def fake_features_w_multipolygons(fake_geoms_w_multipolygons):
    """
    Provides a list of features represented as a dictionary with the Polygon and
    MultiPolygon geometries (as mapping) of the fake_geoms_w_multipolygons fixture.
    """
    return [
        {
            "type": "Feature",
            "id": i,
            "properties": {"population": i * 10},
            "geometry": sg.mapping(geom),
        }
        for i, geom in enumerate(fake_geoms_w_multipolygons, start=1)
    ]
