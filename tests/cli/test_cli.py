"""Unit tests for the pyeac.cli subpackage"""
import pytest

import pyeac.cli


@pytest.mark.parametrize("gridtype", ["hex", "reg"])
class TestCLImain:
    def test_pyeac_run_call(self, gridtype, pyeac_run_mock):
        """
        When using the CLI function pyeac_run is called with input file, output file
        and gridtype.
        """
        pyeac.cli.main(f"{gridtype} -f some_input -o some_output".split(" "))
        pyeac_run_mock.assert_called_with(
            "some_input", "some_output", gridtype=gridtype
        )

    def test_geometries_to_grid_cells_call(
        self, gridtype, geometries_to_grid_cells_mock
    ):
        """
        When using the CLI the function geometries_to_grid_cells receives features
        and bounds from the Collection object returned by fiona.open call and
        gridtype based on the CLI subcommand (hex/reg).
        """
        pyeac.cli.main(f"{gridtype} -f some_input -o some_output".split(" "))
        geometries_to_grid_cells_mock.assert_called_with(
            ["features"], (0, 0, 0, 0), gridtype=gridtype
        )

    def test_properties_to_grid_cells_call(
        self, gridtype, geometries_to_grid_cells_mock, properties_to_grid_cells_mock
    ):
        """
        When using the CLI the function properties_to_grid_cells receives original
        features from the Collection object returned by fiona.open call and new grid
        cells, i.e. the return value from the call to geometries_to_grid_cells.
        """
        pyeac.cli.main(f"{gridtype} -f some_input -o some_output".split(" "))
        properties_to_grid_cells_mock.assert_called_with(
            ["features"], geometries_to_grid_cells_mock.return_value
        )

    def test_fiona_open_writemode_call(
        self, gridtype, fiona_mock, fiona_read_mock,
    ):
        """
        When using the CLI a call to fiona.open in write mode opens a Collection
        object that receives the output file and meta information from the fiona.open
        read mode call.
        """
        pyeac.cli.main(f"{gridtype} -f some_input -o some_output".split(" "))

        args, kwargs = fiona_mock.open.call_args_list[1]
        assert args == ("some_output.geojson", "w")
        assert kwargs == fiona_read_mock.meta

    def test_fiona_open_writemode_Collection_write_call(
        self, gridtype, properties_to_grid_cells_mock, fiona_write_mock, mocker,
    ):
        """
        When using the CLI a call to fiona.open in write mode opens a Collection
        object whose write-method is called for every feature returned by
        properties_to_grid_cells.
        """
        pyeac.cli.main(f"{gridtype} -f some_input -o some_output".split(" "))

        assert fiona_write_mock.write.call_count == len(
            properties_to_grid_cells_mock.return_value
        )
        fiona_write_mock.write.assert_has_calls(
            [mocker.call(rv) for rv in properties_to_grid_cells_mock.return_value]
        )
