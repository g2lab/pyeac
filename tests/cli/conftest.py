import pytest


@pytest.fixture(autouse=True)
def geometries_to_grid_cells_mock(mocker):
    """Mock object for geometries_to_grid_cells function used in pyeac.cli

    The real function returns a list of square or hexagonal geometries, which we fake
    here with simple strings that are our sentinel values.
    """
    mock = mocker.Mock(name="geometries_to_grid_cells_mock")
    mock.return_value = ["geom1", "geom2" "geom3"]
    mocker.patch("pyeac.cli.geometries_to_grid_cells", mock)
    return mock


@pytest.fixture(autouse=True)
def properties_to_grid_cells_mock(mocker):
    """Mock object for properties_to_grid_cells used in pyeac_cli

    The real function returns a list of dictionaries that are the new features, which we
    fake here with simple strings for the geometries and an empty dictionary for the
    properties. As we only want a sentinel value this is sufficient.
    """
    mock = mocker.Mock(name="properties_to_grid_cells_mock")
    mock.return_value = [
        {"geometry": "geom1", "properties": {}},
        {"geometry": "geom2", "properties": {}},
        {"geometry": "geom3", "properties": {}},
    ]
    mocker.patch("pyeac.cli.properties_to_grid_cells", mock)
    return mock


@pytest.fixture
def pyeac_run_mock(mocker):
    """Mock object for pyeac_run function in pyeac_cli"""
    mock = mocker.Mock(name="pyeac_run_mock")
    mocker.patch("pyeac.cli.pyeac_run", mock)
    return mock


@pytest.fixture(autouse=True)
def fiona_mock(mocker):
    """Mock object for fiona module imported in pyeac.cli"""
    mock = mocker.MagicMock(name="fiona_mock")
    mocker.patch("pyeac.cli.fiona", mock)
    return mock


@pytest.fixture(autouse=True)
def shapely_mock(mocker):
    """Mock object for shapely.geometry module imported in pyeac.cli"""
    mock = mocker.MagicMock(name="shapely_mock")
    mocker.patch("pyeac.cli.sg", mock)
    return mock


@pytest.fixture
def fiona_read_mock(mocker):
    """Mock object for calling fiona.open in read mode.

    Provide some initial values for a fake dataset.
    """
    fiona_open_read = mocker.MagicMock(name="fiona_open_read_mock")
    # fiona.Collection objects are either directly used in for loops or in calls to
    # list() so we need to have an __iter__ method that "returns" the features from our
    # dataset.
    fiona_open_read.__iter__.return_value = ["features"]
    fiona_open_read.bounds = (0, 0, 0, 0)
    fiona_open_read.meta = {
        "schema": {"geometry": "GeomTypeXYZ", "properties": "PropertiesMappingXYZ"},
        "driver": "DriverXYZ",
        "crs": "CRS_XYZ",
    }
    return fiona_open_read


@pytest.fixture
def fiona_write_mock(mocker):
    """Mock object for calling fiona.open in write mode."""
    return mocker.Mock(name="fiona_open_write_mock")


@pytest.fixture(autouse=True)
def fiona_open_mock(fiona_mock, fiona_read_mock, fiona_write_mock, mocker):
    """Mock fiona.open calls in with statements.

    In this special case we want subsequent calls to fiona.open in with statements to
    return different mock objects. The first call returns a mock for calling fiona.open
    in read mode whereas the second call returns a mock for calling fiona.open in write
    mode. This way we can make assertions on the single mock objects more easily.
    """
    fiona_open_mock = mocker.MagicMock(name="fiona_open_mock")
    fiona_open_mock.__enter__ = mocker.Mock(
        name="fiona_open_enter_mock", side_effect=[fiona_read_mock, fiona_write_mock]
    )
    fiona_open_mock.__exit__ = mocker.Mock(
        name="fiona_open_exit_mock", return_value=False
    )
    fiona_mock.open.return_value = fiona_open_mock
    return fiona_mock
