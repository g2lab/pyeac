"""pyeac cli"""
import sys

import fiona
import shapely.geometry as sg

import pyeac
from pyeac.cli.parsers import parser
from pyeac.cli.utils import CmdAction
from pyeac.core import geometries_to_grid_cells, properties_to_grid_cells


def pyeac_run(file, output, *, gridtype):
    """Run core functions on an input file and write the results to a new output file

    The opening and writing files fiona is used.

    Args:
        file (str): Path to an input file.
        output (str): Path to an output file.
        gridtype (str): The gridtype to use. Can be "reg" or "hex".
    """
    with fiona.open(file) as inp:
        features = list(inp)
        bounds = inp.bounds
        meta = inp.meta
    new_features = properties_to_grid_cells(
        features, geometries_to_grid_cells(features, bounds, gridtype=gridtype)
    )
    meta["schema"]["geometry"] = "Polygon"
    meta["driver"] = "GeoJSON"
    with fiona.open(f"{output}.geojson", "w", **meta) as out:
        for feature in new_features:
            feature["geometry"] = sg.mapping(feature["geometry"])
            out.write(feature)


def main(args=sys.argv[1:]):
    """Main entry point for the CLI

    Args:
        args (list, optional): List of command line arguments. Defaults to sys.argv[1:].
    """
    if "--debug" in args:
        pyeac.logger.setLevel(pyeac.logging.DEBUG)
    if len(args) == 0 and "--debug" not in args:
        parser.print_help()

    parsed = parser.parse_args(args)

    actions = CmdAction(parsed)

    actions.add_multiple(
        [
            (
                "reg",
                lambda file, output: pyeac_run(file, output, gridtype="reg"),
                ["file", "output"],
            ),
            (
                "hex",
                lambda file, output: pyeac_run(file, output, gridtype="hex"),
                ["file", "output"],
            ),
        ]
    )

    if parsed.cmd:
        actions.do(parsed.cmd)


if __name__ == "__main__":
    main()
