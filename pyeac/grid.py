"""Provides classes for generating grids and grid cells"""

import math

import numpy as np
import shapely.geometry as sg


class Grid:
    """Represents a grid

    This base class provides the methods for creating the x and y coordinates of a grid
    based on a bounding box and a given cellsize. It is not meant to be instantiated
    directly. The main purpose is to have a uniform way of creating and accessing grid
    coordinates.

    Attributes:
        X_SPACING (float, int): A constant that scales the spacing of the grid cells in
            x-direction. Needs to be set per class. Defaults to None.
        Y_SPACING (float, int): A constant that scales the spacing of the grid cells in
            y-direction. Needs to be set per class. Defaults to None.
        bounds (tuple): A tuple in the form (min_x, min_y, max_x, max_y) denoting the
            extent of the grid
        cellsize (float, int): The difference between grid points (grid spacing)
        min_x (float, int): Lower left extent on x-axis
        min_y (float, int): Lower left extent on y-axis
        max_x (float, int): Upper right extent on x-axis
        max_y (float, int): Upper right extent on y-axis
        self.xx (np.ndarray): The x-coordinates of the grid
        self.yy (np.ndarray): the y-coordinates of the grid
    """

    X_SPACING = None
    Y_SPACING = None

    def __init__(self, bounds, cellsize):
        """Initializes Grid with bounds and cellsize"""
        self.bounds = bounds
        self.min_x, self.min_y, self.max_x, self.max_y = bounds
        self.cellsize = cellsize
        self.xx, self.yy = self._make_grid()

    def _make_grid(self):
        """Uses np.meshgrid to create a grid of x- and y-coordinates

        Returns:
            tuple: A tuple containing two np.ndarrays with the x- resp. y-coordinates of
                the created grid.
        """
        XX, YY = np.meshgrid(
            np.arange(self.min_x, self.max_x, self.X_SPACING),
            np.arange(self.min_y, self.max_y, self.Y_SPACING),
        )
        return XX, YY

    @property
    def coords(self):
        """Return the grid coordinates as a np.ndarray of x- and y-coordinates

        Returns:
            np.ndarray: An array of shape (n, 2) where n is the amount of grid points.
        """
        return np.vstack([self.xx.ravel(), self.yy.ravel()]).T


class RegularGrid(Grid):
    """Represents a regular grid

    Attributes:
        X_SPACING (float, int): A constant that scales the spacing of the grid cells in
            x-direction. For a regular grid this equals to the cellsize.
        Y_SPACING (float, int): A constant that scales the spacing of the grid cells in
            y-direction. For a regular grid this equals to the cellsize.
        bounds (tuple): A tuple in the form (min_x, min_y, max_x, max_y) denoting the
            extent of the grid.
        cellsize (float, int): The difference between grid points (grid spacing).
    """

    def __init__(self, bounds, cellsize):
        """Initializes RegularGrid with bounds and cellsize"""
        self.X_SPACING = cellsize
        self.Y_SPACING = cellsize
        super().__init__(bounds, cellsize)


class HexGrid(Grid):
    """Represents a hexagonal grid

    Attributes:
        X_SPACING (float, int): A constant that scales the spacing of the grid cells in
            x-direction. For a hexagonal grid this equals to cellsize * math.sqrt(3) / 2
        Y_SPACING (float, int): A constant that scales the spacing of the grid cells in
            y-direction. For a hexagonal grid this equals to cellsize * 0.75.
        bounds (tuple): A tuple in the form (min_x, min_y, max_x, max_y) denoting the
            extent of the grid.
        cellsize (float, int): The difference between grid points (grid spacing).
    """

    def __init__(self, bounds, cellsize):
        """Initializes HexGrid with bounds and cellsize"""
        self.X_SPACING = cellsize * math.sqrt(3) / 2
        self.Y_SPACING = cellsize * 0.75
        super().__init__(bounds, cellsize)

    def _make_grid(self):
        """Creates a grid and then shifts every second row of the grid two the right

        Returns:
            tuple: A tuple containing two np.ndarrays with the x- resp. y-coordinates of
                the created grid.
        """
        XX, YY = super()._make_grid()
        # In a hexagonal grid every second row is shifted to the right.
        XX[1::2] += self.cellsize / 2 * math.sqrt(3) / 2
        return XX, YY


class GridCell(sg.Polygon):
    """Represents a grid cell

    This base class provides means for creating a grid cell on a given center point
    coordinate with given size. It is not meant to be instantiated directly. The created
    grid cell is a shapely.geometry.Polygon instance.

    Arguments:
        x (float, int): The x-coordinate of the centroid.
        y (float, int): The y-coordinate of the centroid.
        size (float, int): The size of the grid cell.
    """

    def __init__(self, x, y, size):
        """Initializes GridCell with x, y and size"""
        self.x = x
        self.y = y
        self.size = size
        super().__init__(shell=self.cell_coords())

    def cell_coords(self):
        """Calculate cell coordinates based on given x- and y-coordinates and size"""
        raise NotImplementedError


class Hexagon(GridCell):
    """Represents a hexagonal grid cell

    Arguments:
        x (float, int): The x-coordinate of the centroid.
        y (float, int): The y-coordinate of the centroid.
        size (float, int): The size of the grid cell. Denotes the long diameter of a
            hexagonal cell.
    """

    def cell_coords(self):
        """Calculate hexagonal cell coordinates

        Returns:
            list: A list of tuples that contain the x- and y-coordinates of the hexagons
            vertices.
        """
        return [
            (
                self.x + math.cos(math.radians(angle)) * (self.size / 2),
                self.y + math.sin(math.radians(angle)) * (self.size / 2),
            )
            for angle in range(30, 360, 60)
        ]


class Regular(GridCell):
    """Represents a regular/square grid cell

    Arguments:
        x (float, int): The x-coordinate of the centroid.
        y (float, int): The y-coordinate of the centroid.
        size (float, int): The size of the grid cell. Denotes the length of one edge of
            a square.
    """

    def cell_coords(self):
        """Calculate regular/square cell coordinates

        Returns:
            list: A list of tuples that contain the x- and y-coordinates of the squares
            vertices.
        """
        return [
            (
                self.x + math.cos(math.radians(angle)) * (self.size / 2) * math.sqrt(2),
                self.y + math.sin(math.radians(angle)) * (self.size / 2) * math.sqrt(2),
            )
            for angle in range(45, 360, 90)
        ]


GRIDS = {"reg": (RegularGrid, Regular), "hex": (HexGrid, Hexagon)}
