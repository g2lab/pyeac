"""pyeac core functionality"""
import logging

import numpy as np
import scipy.optimize as spo
import shapely.vectorized as sv

from pyeac.grid import GRIDS, GridCell
from pyeac.util import distance_matrix, initial_cellsize, as_single_multipolygon

logger = logging.getLogger("pyeac.core")

__all__ = ["geometries_to_grid_cells", "properties_to_grid_cells"]


def geometries_to_grid_cells(features, bounds, gridtype="hex", growth_rate=0.03):
    """Creates a regular or hexagonal grid representation of feature geometries

    Based on the given bounds and initial cellsize a grid of specified gridtype is
    created. The cellsize is then scaled up or down by the growth rate until the amount
    of grid points that lie inside the given features spatial extent is equal the amount
    of feature geometries. Then the coordinates of the contained points are used
    together with the cellsize to create the new grid cell geometries.

    Args:
        features (list): List of features as GeoJSON-like mapping
        bounds (tuple): A tuple in the form (min_x, min_y, max_x, max_y) describing the
            extent of the dataset.
        gridtype (str, optional): The used gridtype. Can be "reg" or "hex".
            Defaults to "hex".
        growth_rate (float, optional): The rate at which the cellsize is scaled up/down.
            Defaults to 0.03.

    Returns:
        list: A list of pyeac.grid.GridCell instances (:class:`pyeac.grid.Hexagon` or
        :class:`pyeac.grid.Regular`).
    """
    feat_count = len(features)
    geom = as_single_multipolygon(features)
    cellsize = initial_cellsize(bounds)
    Grid, Cell = GRIDS[gridtype]
    while True:
        grid = Grid(bounds, cellsize)
        iscontained = sv.contains(geom, *np.hsplit(grid.coords, 2))
        contained_idx, _ = np.where(iscontained)
        if len(contained_idx) == feat_count:
            break
        elif len(contained_idx) > feat_count:
            cellsize *= 1 + growth_rate
        elif len(contained_idx) < feat_count:
            cellsize *= 1 - growth_rate
    return [Cell(x, y, cellsize) for x, y in grid.coords[contained_idx]]


def properties_to_grid_cells(features, grid):
    """Creates new features from original features and corresponding grid cells

    The assignment of the original features properties to the new grid cell features is
    done by using the Hungarian algorithm (Kuhn-Munkres-algorithm). A costmatrix is
    built up that uses the distances of all original geometries to all grid cells. This
    costmatrix is the input to spo.linear_sum_assignment, which then tries to assign
    each original feature to a grid cell with minimal cost, in this case the minimal
    distance.

    Args:
        features (list): List of features as GeoJSON-like mapping
        grid (list): A list of :class:`pyeac.grid.GridCell` instances returned from
            geometries_to_grid_cells.

    Returns:
        list: List of features with new grid cell geometries as GeoJSON-like mapping

    Raises:
        ValueError: If `len(features)` is not equal to `len(grid)`.
        TypeError: If grid contains elements that are not an instance of
            :class:`pyeac.grid.GridCell`.
    """
    if len(features) != len(grid):
        raise ValueError(
            "'features' and 'grid' must have the same length: "
            f"{len(features)} != {len(grid)}"
        )
    if not all(isinstance(cell, GridCell) for cell in grid):
        raise TypeError("Only instances of 'GridCell' can be used for the assignment!")
    costmatrix = distance_matrix(features, grid)
    rows, cols = spo.linear_sum_assignment(costmatrix)
    return [
        {
            "type": "Feature",
            "id": i,
            "properties": features[row]["properties"],
            "geometry": grid[col],
        }
        for i, (row, col) in enumerate(zip(rows, cols))
    ]
